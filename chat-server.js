function StringSet() {
    var setObj = {}, val = {};

    this.add = function(str) {
        setObj[str] = val;
    };

    this.contains = function(str) {
        return setObj[str] === val;
    };

    this.remove = function(str) {
        delete setObj[str];
    };

    this.values = function() {
        var values = [];
        for (var i in setObj) {
            if (setObj[i] === val) {
                values.push(i);
            }
        }
        return values;
    };
}


var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");
var chatrooms = new StringSet();
var chatpasswords = {};
var chatroomsOwners = {};
var chatusers = {};
var chatroomBans = {};
var passwordRooms = new StringSet();
var avatars = {};
 
 //Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
 var app_client = http.createServer(function(req, resp){
 	// This callback runs when a new connection is made to our HTTP server.
 	
 	 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
	 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app_client.listen(3456);
 
 
 // Do the Socket.IO magic:
 var io = socketio.listen(app_client);
 io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
		console.log("message: "+data["message"]); // log it to the Node.JS output
		io.sockets.emit("message_to_client",{message:data["message"]}) // broadcast the message to other users
	});
	socket.on("toServer_login",function(data){
		console.log("image for user: "+data['user']+"    : "+data['image']);
		avatars[data['user']] = data['image'];
	});
	socket.on('toServer_getChats',function(){
		io.sockets.emit("toClient_createChat",{chats:chatrooms.values(),passrooms:passwordRooms.values()});
	});
	socket.on('toServer_createChat', function(data) {
		// This callback runs when the server receives a new message from the client.
		console.log("message: "+data["chatroom"]); // log it to the Node.JS output
		chatpasswords[data['chatroom']] = data['password'];
		chatroomsOwners[data['chatroom']] = data['owner'];
		chatrooms.add(data['chatroom']);
		if (data['password'] != ""){passwordRooms.add(data['chatroom']);}
		var passrooms = passwordRooms.values();
		var chatrs = chatrooms.values();
		io.sockets.emit("toClient_createChat",{chats:chatrs,passrooms:passwordRooms.values()}) // broadcast the message to other users
	});
	socket.on('toServer_chatMessage', function(data) {
		// This callback runs when the server receives a new message from the client.
		console.log("message: "+data["chatmessage"]); // log it to the Node.JS output
		io.sockets.emit("toClient_chatMessage",{username:data['username'],message:data["chatmessage"]}) // broadcast the message to other users
	});
	socket.on('toServer_enterChat',function(data){
		console.log("server-side username: "+data['username']);
		if (typeof chatroomBans[data['chatroom']] != 'undefined' && chatroomBans[data['chatroom']].contains(data['username'])){
			io.sockets.emit("toClient_blockedChat",{chatroom:data['chatroom'],user:data['username']});
		}else{
			if (typeof chatusers[data['chatroom']] == 'undefined'){chatusers[data['chatroom']] = new StringSet();}
			if (passwordRooms.contains(data['chatroom']) && chatpasswords[data['chatroom']] !== data['password']){
				io.sockets.emit("toClient_wrongPassword",{username:data['username'],chatroom:data['chatroom']});	
			}else{
				chatusers[data['chatroom']].add(data['username']);
				var usersarray = chatusers[data['chatroom']].values();
				io.sockets.emit("toClient_enterChat",{owner:chatroomsOwners[data['chatroom']],chatroom:data['chatroom'],users:usersarray,useravatars:avatars});
			}
		}
	});
	socket.on("toServer_exitChat",function(data){
		console.log("chat: "+data['chatroomname']);
		console.log("user: "+data['username']);
		chatusers[data['chatroomname']].remove(data['username']);
		var usersarray = chatusers[data['chatroomname']].values();
		io.sockets.emit("toClient_enterChat",{chatroom:data['chatroomname'],users:usersarray});
	});
	socket.on("toServer_private",function(data){
		console.log("server-side firstuser: "+data['firstuser']);
		console.log("server-side seconduser: "+data['seconduser']);
		io.sockets.emit("toClient_private",{firstuser:data['firstuser'],seconduser:data['seconduser']});
	});
	socket.on("toServer_tempBanUser",function(data){
		chatusers[data['chatroom']].remove(data['user']);
		var usersarray = chatusers[data['chatroom']].values();
		io.sockets.emit("toClient_banUser",{chatroom:data['chatroom'],users:usersarray,banneduser:data['user'],owner:chatroomsOwners[data['chatroom']]});
	});
	socket.on("toServer_banUser",function(data){
		if (typeof chatroomBans[data['chatroom']] == 'undefined'){chatroomBans[data['chatroom']] = new StringSet();}
		chatusers[data['chatroom']].remove(data['user']);
		chatroomBans[data['chatroom']].add(data['user']);
		var usersarray = chatusers[data['chatroom']].values();
		io.sockets.emit("toClient_banUser",{chatroom:data['chatroom'],users:usersarray,banneduser:data['user'],owner:chatroomsOwners[data['chatroom']]});
	});
	socket.on("toServer_acceptPrivate",function(data){
		io.sockets.emit("toClient_acceptPrivate",{firstuser:data['firstuser'],seconduser:data['seconduser']});
	});
	socket.on("toServer_rejectPrivate",function(data){
		io.sockets.emit("toClient_rejectPrivate",{user:data['user']});
	});
});

var app_private = http.createServer(function(req, resp){
 	// This callback runs when a new connection is made to our HTTP server.
 	
 	 
	fs.readFile("privateDisplay.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
	 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app_private.listen(3457);

 var io_private = socketio.listen(app_private);
 io_private.sockets.on("connection", function(socket){

	socket.on("toServer_privateMessage",function(data){
		console.log("message received");
		console.log("firstuser: "+data['firstuser']);
		console.log("message: "+data['chatmessage']);
		io_private.sockets.emit("toClient_privateMessage",{firstuser:data['firstuser'],seconduser:data['seconduser'],message:data['chatmessage']});
	});
});
